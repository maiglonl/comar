@extends('layouts.app')

@section('content')
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Erro!</div>

				<div class="card-body">
					<h3>Você não tem permissão para acessar esta página</h3>
				</div>
			</div>
		</div>
	</div>
@endsection
